import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";
export const useLoginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const loginName = ref("");
  const messageStore = useMessageStore();
  const errorMessage = ref("");
  const isLogin = computed(() => {
    //Login is not empty.
    return loginName.value !== "";
  });
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      messageStore.showMessage("Login or Password not correct!!");
    }
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData, errorMessage };
});
